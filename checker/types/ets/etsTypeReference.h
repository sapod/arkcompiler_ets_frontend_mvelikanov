/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ES2PANDA_COMPILER_CHECKER_TYPES_ETS_TYPE_REFERENCE_H
#define ES2PANDA_COMPILER_CHECKER_TYPES_ETS_TYPE_REFERENCE_H

#include "plugins/ecmascript/es2panda/checker/types/type.h"

namespace panda::es2panda::binder {
class LocalVariable;
}  // namespace panda::es2panda::binder

namespace panda::es2panda::checker {
class ETSTypeReference : public Type {
public:
    explicit ETSTypeReference(ArenaAllocator *allocator, Type **ref, Type **assembler_ref,
                              binder::LocalVariable *var_ref)
        : Type(TypeFlag::ETS_TYPE_REFERENCE),
          ref_(ref),
          assembler_ref_(assembler_ref),
          var_ref_(var_ref),
          constraints_(allocator->Adapter())
    {
    }

    Type *Ref()
    {
        return *ref_;
    }

    const Type *Ref() const
    {
        return *ref_;
    }

    void SetRefType(Type *const type)
    {
        *ref_ = type;
    }

    Type *GetAssemblerRef() const
    {
        return *assembler_ref_;
    }

    binder::LocalVariable *VarRef()
    {
        return var_ref_;
    }

    const binder::LocalVariable *VarRef() const
    {
        return var_ref_;
    }

    bool HasRefType() const
    {
        return (*ref_) != nullptr;
    }

    ArenaVector<Type *> GetConstraints()
    {
        return constraints_;
    }

    void AddConstraint(Type *constraint_type)
    {
        constraints_.emplace_back(constraint_type);
    }

    util::StringView ReferencedName() const;

    void ToString(std::stringstream &ss) const override;
    void ToAssemblerType(std::stringstream &ss) const override;
    void Identical(TypeRelation *relation, Type *other) override;
    bool AssignmentSource([[maybe_unused]] TypeRelation *relation, [[maybe_unused]] Type *target) override;
    void AssignmentTarget(TypeRelation *relation, Type *source) override;
    Type *Instantiate(ArenaAllocator *allocator, TypeRelation *relation, GlobalTypesHolder *global_types) override;
    void ToDebugInfoType(std::stringstream &ss) const override;

private:
    Type **ref_;
    Type **assembler_ref_;
    binder::LocalVariable *var_ref_ {};
    ArenaVector<Type *> constraints_;
};
}  // namespace panda::es2panda::checker

#endif /* ES2PANDA_COMPILER_CHECKER_TYPES_TS_TYPE_REFERENCE_H */
