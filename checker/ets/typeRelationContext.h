/**
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef ES2PANDA_COMPILER_CHECKER_ETS_TYPE_RELATION_CONTEXT_H
#define ES2PANDA_COMPILER_CHECKER_ETS_TYPE_RELATION_CONTEXT_H

#include "plugins/ecmascript/es2panda/ir/expression.h"
#include "plugins/ecmascript/es2panda/ir/base/classDefinition.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsTypeParameterInstantiation.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsTypeParameterDeclaration.h"
#include "plugins/ecmascript/es2panda/ir/ts/tsInterfaceDeclaration.h"
#include "plugins/ecmascript/es2panda/checker/types/type.h"
#include "plugins/ecmascript/es2panda/checker/ETSchecker.h"

namespace panda::es2panda::checker {
class ETSChecker;

class AssignmentContext {
public:
    AssignmentContext(TypeRelation *relation, ir::Expression *node, Type *source, Type *target,
                      const lexer::SourcePosition &pos, std::initializer_list<TypeErrorMessageElement> list,
                      TypeRelationFlag flags = TypeRelationFlag::NONE)
    {
        flags_ |= ((flags & TypeRelationFlag::NO_BOXING) != 0) ? TypeRelationFlag::NONE : TypeRelationFlag::BOXING;
        flags_ |= ((flags & TypeRelationFlag::NO_UNBOXING) != 0) ? TypeRelationFlag::NONE : TypeRelationFlag::UNBOXING;
        flags_ |= ((flags & TypeRelationFlag::NO_WIDENING) != 0) ? TypeRelationFlag::NONE : TypeRelationFlag::WIDENING;

        auto *const ets_checker = relation->GetChecker()->AsETSChecker();

        auto *const ref_source = ets_checker->GetReferredTypeFromETSTypeReference(source);
        auto *const ref_target = ets_checker->GetReferredTypeFromETSTypeReference(target);

        if (ref_target->IsETSArrayType()) {
            if (node->IsArrayExpression()) {
                ValidateArrayTypeInitializerByElement(relation, node->AsArrayExpression(),
                                                      ref_target->AsETSArrayType());
                return;
            }

            if (node->IsNullLiteral()) {
                flags_ |= TypeRelationFlag::NO_THROW;
            }
        }

        flags_ |= flags;
        relation->SetNode(node);

        if (ref_source->HasTypeFlag(TypeFlag::CONSTANT)) {
            flags_ |= TypeRelationFlag::NARROWING;
        }

        relation->SetFlags(flags_);

        if (!relation->IsAssignableTo(ref_source, ref_target)) {
            if (((flags_ & TypeRelationFlag::UNBOXING) != 0) && ref_source->IsETSObjectType() && !relation->IsTrue()) {
                ets_checker->CheckUnboxedTypesAssignable(relation, ref_source, ref_target);
            }
            if (((flags_ & TypeRelationFlag::BOXING) != 0) && ref_target->IsETSObjectType() && !relation->IsTrue()) {
                ets_checker->CheckBoxedSourceTypeAssignable(relation, ref_source, ref_target);
            }
        }

        if (!relation->IsTrue() && (flags_ & TypeRelationFlag::NO_THROW) == 0) {
            relation->RaiseError(list, pos);
        }

        relation->SetNode(nullptr);
        relation->SetFlags(TypeRelationFlag::NONE);
        assignable_ = true;
    }

    bool IsAssignable() const
    {
        return assignable_;
    }

    void ValidateArrayTypeInitializerByElement(TypeRelation *relation, ir::ArrayExpression *node, ETSArrayType *target);

private:
    TypeRelationFlag flags_ = TypeRelationFlag::IN_ASSIGNMENT_CONTEXT;
    bool assignable_ {false};
};

class InvocationContext {
public:
    InvocationContext(TypeRelation *relation, ir::Expression *node, Type *source, Type *target,
                      const lexer::SourcePosition &pos, std::initializer_list<TypeErrorMessageElement> list,
                      TypeRelationFlag initial_flags = TypeRelationFlag::NONE)
    {
        flags_ |=
            ((initial_flags & TypeRelationFlag::NO_BOXING) != 0) ? TypeRelationFlag::NONE : TypeRelationFlag::BOXING;
        flags_ |= ((initial_flags & TypeRelationFlag::NO_UNBOXING) != 0) ? TypeRelationFlag::NONE
                                                                         : TypeRelationFlag::UNBOXING;

        auto *const ets_checker = relation->GetChecker()->AsETSChecker();

        auto *const ref_source = ets_checker->GetReferredTypeFromETSTypeReference(source);
        auto *const ref_target = ets_checker->GetReferredTypeFromETSTypeReference(target);

        relation->SetNode(node);
        relation->SetFlags(flags_ | initial_flags);

        if (!relation->IsAssignableTo(ref_source, ref_target)) {
            if (((flags_ & TypeRelationFlag::UNBOXING) != 0U) && ref_source->IsETSObjectType() && !relation->IsTrue()) {
                ets_checker->CheckUnboxedSourceTypeWithWideningAssignable(relation, ref_source, ref_target);
            }
            if (((flags_ & TypeRelationFlag::BOXING) != 0) && ref_target->IsETSObjectType() && !relation->IsTrue()) {
                ets_checker->CheckBoxedSourceTypeAssignable(relation, ref_source, ref_target);
            }
        }

        relation->SetNode(nullptr);
        relation->SetFlags(TypeRelationFlag::NONE);

        if (!relation->IsTrue()) {
            if ((initial_flags & TypeRelationFlag::NO_THROW) == 0) {
                relation->RaiseError(list, pos);
            }
            return;
        }

        invocable_ = true;
    }

    bool IsInvocable() const
    {
        return invocable_;
    }

private:
    TypeRelationFlag flags_ = TypeRelationFlag::NONE;
    bool invocable_ {false};
};

class InstantiationContext {
public:
    InstantiationContext(ETSChecker *checker, ETSObjectType *type, ir::TSTypeParameterInstantiation *type_args,
                         const lexer::SourcePosition &pos)
        : checker_(checker), type_arg_vars_(checker->Allocator()->Adapter())
    {
        ir::TSTypeParameterDeclaration *type_param_decl = nullptr;

        if (type->HasObjectFlag(ETSObjectFlags::CLASS)) {
            type_param_decl = type->GetDeclNode()->AsClassDefinition()->TypeParams();
        } else if (type->HasObjectFlag(ETSObjectFlags::INTERFACE)) {
            type_param_decl = type->GetDeclNode()->AsTSInterfaceDeclaration()->TypeParams();
        }
        if (ValidateTypeArguments(type, type_param_decl, type_args, pos)) {
            return;
        }

        InstantiateType(type, type_param_decl, type_args);
    }

    InstantiationContext(ETSChecker *checker, ETSObjectType *type, ArenaVector<Type *> &type_args)
        : checker_(checker), type_arg_vars_(checker->Allocator()->Adapter())
    {
        ir::TSTypeParameterDeclaration *type_param_decl = nullptr;

        if (type->HasObjectFlag(ETSObjectFlags::CLASS)) {
            type_param_decl = type->GetDeclNode()->AsClassDefinition()->TypeParams();
        } else if (type->HasObjectFlag(ETSObjectFlags::ENUM)) {
            return;
        } else {
            ASSERT(type->HasObjectFlag(ETSObjectFlags::INTERFACE));
            type_param_decl = type->GetDeclNode()->AsTSInterfaceDeclaration()->TypeParams();
        }

        InstantiateType(type, type_param_decl, type_args);
    }

    ETSObjectType *Result()
    {
        return result_;
    }

private:
    bool ValidateTypeArguments(ETSObjectType *type, ir::TSTypeParameterDeclaration *type_param_decl,
                               ir::TSTypeParameterInstantiation *type_args, const lexer::SourcePosition &pos);
    void InstantiateType(ETSObjectType *type, ir::TSTypeParameterDeclaration *type_param_decl,
                         ir::TSTypeParameterInstantiation *type_args);

    void InstantiateType(ETSObjectType *type, ir::TSTypeParameterDeclaration *type_param_decl,
                         ArenaVector<Type *> &type_arg_types);
    util::StringView GetHashFromTypeArguments(ArenaVector<Type *> &type_arg_types);

    ETSChecker *checker_;
    ArenaVector<binder::LocalVariable *> type_arg_vars_;
    ETSObjectType *result_ {};
};

}  // namespace panda::es2panda::checker

#endif
