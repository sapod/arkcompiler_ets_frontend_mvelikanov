/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import * as ts from 'typescript';
import { ProblemInfo, TypeScriptLinter } from './TypeScriptLinter';
import { AutofixInfo } from './Common';

const statementKinds = [
  ts.SyntaxKind.Block,ts.SyntaxKind.EmptyStatement,ts.SyntaxKind.VariableStatement,ts.SyntaxKind.ExpressionStatement,
  ts.SyntaxKind.IfStatement,ts.SyntaxKind.DoStatement,ts.SyntaxKind.WhileStatement,ts.SyntaxKind.ForStatement,
  ts.SyntaxKind.ForInStatement,ts.SyntaxKind.ForOfStatement,ts.SyntaxKind.ContinueStatement,
  ts.SyntaxKind.BreakStatement,ts.SyntaxKind.ReturnStatement,ts.SyntaxKind.WithStatement,
  ts.SyntaxKind.SwitchStatement,ts.SyntaxKind.LabeledStatement,ts.SyntaxKind.ThrowStatement,
  ts.SyntaxKind.TryStatement,ts.SyntaxKind.DebuggerStatement,
];

export function isStatementKindNode(tsNode: ts.Node): boolean {
  return statementKinds.includes(tsNode.kind);
}

export function isAssignmentOperator(tsBinOp: ts.BinaryOperatorToken): boolean {
  return tsBinOp.kind >= ts.SyntaxKind.FirstAssignment && tsBinOp.kind <= ts.SyntaxKind.LastAssignment;
}

export function isArrayNotTupleType(tsType: ts.TypeNode | undefined): boolean {
  if (tsType && tsType.kind && ts.isArrayTypeNode(tsType)) {
    // Check that element type is not a union type to filter out tuple types induced by tuple literals.
    const tsElemType = unwrapParenthesizedType(tsType.elementType);
    return !ts.isUnionTypeNode(tsElemType);
  }

  return false;
}

export function isNumberType(tsType: ts.Type): boolean {
  if (tsType.isUnion()) {
    for (let tsCompType of tsType.types) {
      if ((tsCompType.flags & ts.TypeFlags.NumberLike) === 0) return false;
    }
    return true;
  }
  return (tsType.getFlags() & ts.TypeFlags.NumberLike) !== 0;
}

export function isBooleanType(tsType: ts.Type): boolean {
  return (tsType.getFlags() & ts.TypeFlags.BooleanLike) !== 0;
}

export function isStringType(tsType: ts.Type): boolean {
  if (tsType.isUnion()) {
    for (let tsCompType of tsType.types) {
      if ((tsCompType.flags & ts.TypeFlags.StringLike) === 0) return false;
    }
    return true;
  }
  return (tsType.getFlags() & ts.TypeFlags.StringLike) !== 0;
}

export function unwrapParenthesizedType(tsType: ts.TypeNode): ts.TypeNode {
  while (ts.isParenthesizedTypeNode(tsType)) {
    tsType = tsType.type;
  }
  return tsType;
}

export function findParentIf(asExpr: ts.AsExpression): ts.IfStatement | null {
  let node = asExpr.parent;
  while (node) {
    if (node.kind === ts.SyntaxKind.IfStatement) 
      return node as ts.IfStatement;

    node = node.parent;
  }

  return null;
}

export function isDestructuringAssignmentLHS(
  tsExpr: ts.ArrayLiteralExpression | ts.ObjectLiteralExpression
): boolean {
  // Check whether given expression is the LHS part of the destructuring
  // assignment (or is a nested element of destructuring pattern).
  let tsParent = tsExpr.parent;
  let tsCurrentExpr: ts.Node = tsExpr;
  while (tsParent) {
    if (
      ts.isBinaryExpression(tsParent) && isAssignmentOperator(tsParent.operatorToken) &&
      tsParent.left === tsCurrentExpr
    )
      return true;

    if (
      (ts.isForStatement(tsParent) || ts.isForInStatement(tsParent) || ts.isForOfStatement(tsParent)) &&
      tsParent.initializer && tsParent.initializer === tsCurrentExpr
    )
      return true;

    tsCurrentExpr = tsParent;
    tsParent = tsParent.parent;
  }

  return false;
}

export function isEnumType(tsType: ts.Type): boolean {
  // Note: For some reason, test (tsType.flags & ts.TypeFlags.Enum) != 0 doesn't work here.
  // Must use SymbolFlags to figure out if this is an enum type.
  return tsType.symbol && (tsType.symbol.flags & ts.SymbolFlags.Enum) !== 0;
}

export function isObjectLiteralType(tsType: ts.Type): boolean {
  return tsType.symbol && (tsType.symbol.flags & ts.SymbolFlags.ObjectLiteral) !== 0;
}

export function isNumberLikeType(tsType: ts.Type): boolean {
  return (tsType.getFlags() & ts.TypeFlags.NumberLike) !== 0;
}

export function hasModifier(tsModifiers: readonly ts.Modifier[] | undefined, tsModifierKind: number): boolean {
  // Sanity check.
  if (!tsModifiers) return false;

  for (const tsModifier of tsModifiers) {
    if (tsModifier.kind === tsModifierKind) return true;
  }

  return false;
}

export function unwrapParenthesized(tsExpr: ts.Expression): ts.Expression {
  let unwrappedExpr = tsExpr;
  while (ts.isParenthesizedExpression(unwrappedExpr))
    unwrappedExpr = unwrappedExpr.expression;

  return unwrappedExpr;
}

export function symbolHasDuplicateName(symbol: ts.Symbol, tsDeclKind: ts.SyntaxKind): boolean {
  // Type Checker merges all declarations with the same name in one scope into one symbol. 
  // Thus, check whether the symbol of certain declaration has any declaration with
  // different syntax kind.
  const symbolDecls = symbol?.getDeclarations();
  if (symbolDecls) {
    for (const symDecl of symbolDecls) {
      // Don't count declarations with 'Identifier' syntax kind as those
      // usually depict declaring an object's property through assignment.
      if (symDecl.kind !== ts.SyntaxKind.Identifier && symDecl.kind !== tsDeclKind) return true;
    }
  }

  return false;
}

export function isReferenceType(tsType: ts.Type): boolean {
  const f = tsType.getFlags();
  return (
    (f & ts.TypeFlags.InstantiableNonPrimitive) != 0 || (f & ts.TypeFlags.Object) != 0 ||
    (f & ts.TypeFlags.Boolean) != 0 || (f & ts.TypeFlags.Enum) != 0 || (f & ts.TypeFlags.NonPrimitive) != 0 ||
    (f & ts.TypeFlags.Number) != 0 || (f & ts.TypeFlags.String) != 0
  );
}

export function isPrimitiveType(type: ts.Type): boolean {
  const f = type.getFlags();
  return (
    (f & ts.TypeFlags.Boolean) != 0 || (f & ts.TypeFlags.BooleanLiteral) != 0 || 
    (f & ts.TypeFlags.Number) != 0 || (f & ts.TypeFlags.NumberLiteral) != 0 
    // In ArkTS 'string' is not a primitive type. So for the common subset 'string' 
    // should be considered as a reference type. That is why next line is commented out.
    //(f & ts.TypeFlags.String) != 0 || (f & ts.TypeFlags.StringLiteral) != 0
  );
}

export function isTypeSymbol(symbol: ts.Symbol | undefined): boolean {
  return (
    !!symbol && !!symbol.flags &&
    ((symbol.flags & ts.SymbolFlags.Class) !== 0 || (symbol.flags & ts.SymbolFlags.Interface) !== 0)
  );
}

// Check whether type is generic 'Array<T>' type defined in TypeScript standard library.
export function isGenericArrayType(tsType: ts.Type): tsType is ts.TypeReference {
  return (
    isTypeReference(tsType) && tsType.typeArguments?.length === 1 && tsType.target.typeParameters?.length === 1 &&
    tsType.getSymbol()?.getName() === 'Array'
  );
}

export function isTypeReference(tsType: ts.Type): tsType is ts.TypeReference {
  return (
    (tsType.getFlags() & ts.TypeFlags.Object) !== 0 &&
    ((tsType as ts.ObjectType).objectFlags & ts.ObjectFlags.Reference) !== 0
  );
}

export function isNullType(tsTypeNode: ts.TypeNode): boolean {
  return (ts.isLiteralTypeNode(tsTypeNode) && tsTypeNode.literal.kind === ts.SyntaxKind.NullKeyword);
}

export function isThisOrSuperExpr(tsExpr: ts.Expression): boolean {
  return (tsExpr.kind == ts.SyntaxKind.ThisKeyword || tsExpr.kind == ts.SyntaxKind.SuperKeyword);
}

export function isPrototypeSymbol(symbol: ts.Symbol | undefined): boolean {
  return (!!symbol && !!symbol.flags && (symbol.flags & ts.SymbolFlags.Prototype) !== 0);
}

export function isFunctionSymbol(symbol: ts.Symbol | undefined): boolean {
  return (!!symbol && !!symbol.flags && (symbol.flags & ts.SymbolFlags.Function) !== 0);
}

export function isInterfaceType(tsType: ts.Type | undefined): boolean {
  return (
    !!tsType && !!tsType.symbol && !!tsType.symbol.flags &&
    (tsType.symbol.flags & ts.SymbolFlags.Interface) !== 0
  );
}

export function isAnyType(tsType: ts.Type): tsType is ts.TypeReference {
  return (tsType.getFlags() & ts.TypeFlags.Any) !== 0;
}

export function isUnsupportedType(tsType: ts.Type): boolean {
  return (
    !!tsType.flags && ((tsType.flags & ts.TypeFlags.Any) !== 0 || (tsType.flags & ts.TypeFlags.Unknown) !== 0 ||
    (tsType.flags & ts.TypeFlags.Intersection) !== 0) || isUnsupportedUnionType(tsType)
  );
}

export function isUnsupportedUnionType(tsType: ts.Type): boolean {
  if (tsType.isUnion()) {
    let tsCompTypes = tsType.types;
    return (
      tsCompTypes.length !== 2 ||
      ((tsCompTypes[0].flags & ts.TypeFlags.Null) === 0 && (tsCompTypes[1].flags & ts.TypeFlags.Null) === 0)
    );
  }
  return false;
}

export function isFunctionOrMethod(tsSymbol: ts.Symbol | undefined): boolean {
  return (
    !!tsSymbol &&
    ((tsSymbol.flags & ts.SymbolFlags.Function) !== 0 || (tsSymbol.flags & ts.SymbolFlags.Method) !== 0)
  );
}

export function isMethodAssignment(tsSymbol: ts.Symbol | undefined): boolean {
  return (
    !!tsSymbol &&
    ((tsSymbol.flags & ts.SymbolFlags.Method) !== 0 && (tsSymbol.flags & ts.SymbolFlags.Assignment) !== 0)
  );
}

function getDeclaration(tsSymbol: ts.Symbol | undefined): ts.Declaration | null {
  if (tsSymbol && tsSymbol.declarations && tsSymbol.declarations.length > 0)
    return tsSymbol.declarations[0];
  return null;
}

function isVarDeclaration(tsDecl: ts.Declaration): boolean {
  return ts.isVariableDeclaration(tsDecl) && ts.isVariableDeclarationList(tsDecl.parent);
}

export function isValidEnumMemberInit(tsExpr: ts.Expression): boolean {
  if (isIntegerConstantValue(tsExpr.parent as ts.EnumMember))
    return true;

  if (
    ts.isParenthesizedExpression(tsExpr) ||
    (ts.isAsExpression(tsExpr) && tsExpr.type.kind === ts.SyntaxKind.NumberKeyword))
    return isValidEnumMemberInit(tsExpr.expression);

  switch (tsExpr.kind) {
    case ts.SyntaxKind.PrefixUnaryExpression:
      return isPrefixUnaryExprValidEnumMemberInit(tsExpr as ts.PrefixUnaryExpression);
    case ts.SyntaxKind.ParenthesizedExpression:
      return isBinaryExprValidEnumMemberInit(tsExpr as ts.BinaryExpression);
    case ts.SyntaxKind.ConditionalExpression:
      return isConditionalExprValidEnumMemberInit(tsExpr as ts.ConditionalExpression);
    case ts.SyntaxKind.Identifier:
      return isIdentifierValidEnumMemberInit(tsExpr as ts.Identifier);
    default:
      return ts.isPropertyAccessExpression(tsExpr) && isIntegerConstantValue(tsExpr)
  }
}

function isPrefixUnaryExprValidEnumMemberInit(tsExpr: ts.PrefixUnaryExpression): boolean {
  return (isUnaryOpAllowedForEnumMemberInit(tsExpr.operator) && isValidEnumMemberInit(tsExpr.operand));
}

function isBinaryExprValidEnumMemberInit(tsExpr: ts.BinaryExpression): boolean {
  return (
    isBinaryOpAllowedForEnumMemberInit(tsExpr.operatorToken) && isValidEnumMemberInit(tsExpr.left) &&
    isValidEnumMemberInit(tsExpr.right)
  );
}

function isConditionalExprValidEnumMemberInit(tsExpr: ts.ConditionalExpression): boolean {
  return (isValidEnumMemberInit(tsExpr.whenTrue) && isValidEnumMemberInit(tsExpr.whenFalse));
}

function isIdentifierValidEnumMemberInit(tsExpr: ts.Identifier): boolean {
  let tsSymbol = TypeScriptLinter.tsTypeChecker.getSymbolAtLocation(tsExpr);
  let tsDecl = getDeclaration(tsSymbol);
  return (!!tsDecl && isVarDeclaration(tsDecl) && isConst(tsDecl.parent));
}

function isUnaryOpAllowedForEnumMemberInit(tsPrefixUnaryOp: ts.PrefixUnaryOperator): boolean {
  return (
    tsPrefixUnaryOp === ts.SyntaxKind.PlusToken || tsPrefixUnaryOp === ts.SyntaxKind.MinusToken ||
    tsPrefixUnaryOp === ts.SyntaxKind.TildeToken
  );
}

function isBinaryOpAllowedForEnumMemberInit(tsBinaryOp: ts.BinaryOperatorToken): boolean {
  return (
    tsBinaryOp.kind === ts.SyntaxKind.AsteriskToken || tsBinaryOp.kind === ts.SyntaxKind.SlashToken ||
    tsBinaryOp.kind === ts.SyntaxKind.PercentToken || tsBinaryOp.kind === ts.SyntaxKind.MinusToken ||
    tsBinaryOp.kind === ts.SyntaxKind.PlusToken || tsBinaryOp.kind === ts.SyntaxKind.LessThanLessThanToken ||
    tsBinaryOp.kind === ts.SyntaxKind.GreaterThanGreaterThanToken || tsBinaryOp.kind === ts.SyntaxKind.BarBarToken ||
    tsBinaryOp.kind === ts.SyntaxKind.GreaterThanGreaterThanGreaterThanToken ||
    tsBinaryOp.kind === ts.SyntaxKind.AmpersandToken || tsBinaryOp.kind === ts.SyntaxKind.CaretToken ||
    tsBinaryOp.kind === ts.SyntaxKind.BarToken || tsBinaryOp.kind === ts.SyntaxKind.AmpersandAmpersandToken
  );
}

export function isConst(tsNode: ts.Node): boolean {
  return !!(ts.getCombinedNodeFlags(tsNode) & ts.NodeFlags.Const);
}

export function isIntegerConstantValue(
  tsExpr: ts.EnumMember | ts.PropertyAccessExpression | ts.ElementAccessExpression
): boolean {
  const tsConstValue = TypeScriptLinter.tsTypeChecker.getConstantValue(tsExpr);
  return (
    tsConstValue !== undefined && typeof tsConstValue === 'number' &&
    tsConstValue.toFixed(0) === tsConstValue.toString()
  );
}

// Returns true iff typeA is a subtype of typeB
export function relatedByInheritanceOrIdentical(typeA: ts.Type, typeB: ts.Type): boolean {
  if (isTypeReference(typeA) && typeA.target !== typeA) typeA = typeA.target;
  if (isTypeReference(typeB) && typeB.target !== typeB) typeB = typeB.target;
  
  if (typeA === typeB || isObjectType(typeB)) return true;
  if (!typeA.symbol || !typeA.symbol.declarations) return false;

  for (let typeADecl of typeA.symbol.declarations) {
    if (
      (!ts.isClassDeclaration(typeADecl) && !ts.isInterfaceDeclaration(typeADecl)) || 
      !typeADecl.heritageClauses
    ) continue;
    for (let heritageClause of typeADecl.heritageClauses) {
      if (processParentTypes(heritageClause.types, typeB)) return true;
    }
  }

  return false;
}

function processParentTypes(parentTypes: ts.NodeArray<ts.Expression>, typeB: ts.Type): boolean {
  for (let baseTypeExpr of parentTypes) {
    let baseType = TypeScriptLinter.tsTypeChecker.getTypeAtLocation(baseTypeExpr);
    if (baseType && relatedByInheritanceOrIdentical(baseType, typeB)) return true;
  }
  return false;
}

export function isObjectType(tsType: ts.Type): boolean {
  return (
    tsType && tsType.isClassOrInterface() && tsType.symbol && 
    (tsType.symbol.name === 'Object' || tsType.symbol.name === 'object')
  );
}

export function logTscDiagnostic(diagnostics: readonly ts.Diagnostic[], log: (message: any, ...args: any[]) => void) {
  diagnostics.forEach((diagnostic) => {
    let message = ts.flattenDiagnosticMessageText(diagnostic.messageText, '\n');

    if (diagnostic.file && diagnostic.start) {
      const { line, character } = ts.getLineAndCharacterOfPosition(diagnostic.file, diagnostic.start);
      message = `${diagnostic.file.fileName} (${line + 1}, ${character + 1}): ${message}`;
    }

    log(message);
  });
}

export function encodeProblemInfo(problem: ProblemInfo): string {
  return `${problem.problem}%${problem.start}%${problem.end}`;
}

export function decodeAutofixInfo(info: string): AutofixInfo {
  let infos = info.split('%');
  return { problemID: infos[0], start: Number.parseInt(infos[1]), end: Number.parseInt(infos[2]) };
}
