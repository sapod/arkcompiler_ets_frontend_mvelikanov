#  Generic functions must be called with explicit type specialization

Rule ``arkts-no-inferred-generic-params``

**Severity: warning**

Currently, ArkTS does not support inference of type parameters in case of calls
to generic functions. If a function is declared generic, all calls must specify
type parameters explicitly.


## TypeScript


```

    function choose<T>(x: T, y: T): T {
        return Math.random() < 0.5 ? x : y
    }

    let x = choose(10, 20) // Ok
    let y = choose("10", 20) // Compile-time error

```

## ArkTS


```

    function choose<T>(x: T, y: T): T {
        return Math.random() < 0.5 ? x : y
    }

    let x = choose<number>(10, 20) // Ok
    let y = choose<number>("10", 20) // Compile-time error

```


