#  Use explicit types instead of ``any``, ``undefined``, ``unknown``

Rule ``arkts-no-any-undefined-unknown``

**Severity: error**

ArkTS does not support ``any``, ``undefined``, and ``unknown`` types.
Specify types explicitly.


## TypeScript


```

    var x
    console.log(x) // undefined

    var y: any
    console.log(y) // undefined

```

## ArkTS


```

    // All variables should have their types specified explicitly:
    let x: Object = {}
    console.log(x) // {}

```

## See also

- Recipe 013:  Use ``Object[]`` instead of tuples (``arkts-no-tuples``)


