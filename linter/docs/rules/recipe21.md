#  Returning ``this`` type is not supported

Rule ``arkts-no-this-as-return-type``

**Severity: error**

ArkTS does not support the returning ``this`` type. Use explicit type instead.


## TypeScript


```

    interface ListItem {
        getHead(): this
    }

```

## ArkTS


```

    interface ListItem {
        getHead(): ListItem
    }

```


