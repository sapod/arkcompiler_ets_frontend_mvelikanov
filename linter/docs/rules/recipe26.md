#  Specialized signatures are not supported

Rule ``arkts-no-str-literals-in-signatures``

**Severity: error**

Currently, ArkTS does not support specialized signatures as a form of
overloading with special type notation (string literal instead of type).
Use other patterns (e.g., interfaces) instead.


## TypeScript


```

    class MyHTMLDivElement {
        // ...
    }

    class MyHTMLSpanElement {
        // ...
    }

    interface MyHTMLDocument {
        createElement(tagname: "div"): MyHTMLDivElement
        createElement(tagname: "span"): MyHTMLSpanElement
    }

```

## ArkTS


```

    class MyHTMLElement {
        // ...
    }

    class MyHTMLDivElement extends MyHTMLElement {
        // ...
    }

    class MyHTMLSpanElement extends MyHTMLElement {
        // ...
    }

    interface MyHTMLDocument {
        createElement(tagName: string): MyHTMLElement
    }

    class D implements MyHTMLDocument {
        createElement(tagName: string): MyHTMLElement {
            switch (tagName) {
                case "div": return new MyHTMLDivElement()
                case "span": return new MyHTMLSpanElement()
                // ...
            }
        }
    }

```


