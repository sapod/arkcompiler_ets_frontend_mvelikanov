#  ``typeof`` operator is not supported

Rule ``arkts-no-typeof``

**Severity: error**

ArkTS does not support ``typeof`` operator and requires explicit typing.
Use ``instanceof`` as a work-around where applicable. Type can be inferred
from the initializer, but the initial value can be not a default one.


## TypeScript


```

    console.log(typeof 5) // "number"
    console.log(typeof "string") // "string"
    let s = "hello"
    let n: typeof s // n type is string, n == ""
    let b = typeof s == "string" // true

```

## ArkTS


```

    let s = new String("hello")
    let n = s
    let b = s instanceof String // true

```

## See also

- Recipe 001:  Objects with property names that are not identifiers are not supported (``arkts-no-computed-props``)
- Recipe 002:  ``Symbol()`` API is not supported (``arkts-no-symbol``)
- Recipe 052:  Attempt to access an undefined property is a compile-time error (``arkts-no-undefined-prop-access``)
- Recipe 059:  ``delete`` operator is not supported (``arkts-no-delete``)
- Recipe 066:  ``in`` operator is not supported (``arkts-no-in``)
- Recipe 105:  Property-based runtime type checks are not supported (``arkts-no-prop-existence-check``)
- Recipe 109:  Dynamic property declaration is not supported (``arkts-no-dyn-prop-decl``)


