#  Use unique names for types, namespaces, etc.

Rule ``arkts-unique-names``

**Severity: error**

Names for types, namespaces and so on must be unique and distinct from other
names, e.g., variable names.


## TypeScript


```

    let X: string
    type X = number[] // Type alias with the same name as the variable

```

## ArkTS


```

    let X: string
    type T = number[] // X is not allowed here to avoid name collisions

```


