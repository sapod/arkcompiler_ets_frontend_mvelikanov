#  Type widening is not supported

Rule ``arkts-no-widening``

**Severity: warning**

ArkTS does not support widened types because in most cases type widening
is applied to the currently unsupported types ``any``, ``unknown``
and ``undefined``.


## TypeScript


```

    var a = null
    var b = undefined
    var c = {c: 0, y: null}
    var d = [null, undefined]

```

## ArkTS


```

    class C {
        public c: number = 0
        public y: Object | null
    }

    let a: Object | null = null
    let b: Object
    let c: C = {c: 0, y: null}
    let d: Object[] = [null, null]

```


