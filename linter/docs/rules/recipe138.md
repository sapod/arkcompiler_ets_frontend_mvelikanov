#  Utility types are not supported

Rule ``arkts-no-utility-types``

**Severity: error**

Currently ArkTS does not support utility types from TypeScript extensions to the
standard library (``Omit``, ``Partial``, ``Readonly``, ``Record``, ``Pick``,
etc.).


## TypeScript


```

    type Person = {
        name: string
        age: number
        location: string
    }

    type QuantumPerson = Omit<Person, "location">

```

## ArkTS


```

    class Person {
        name: string
        age: number
        location: string
    }

    class QuantumPerson {
        name: string
        age: number
    }

```


