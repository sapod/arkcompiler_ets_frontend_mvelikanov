#  Structural identity is not supported

Rule ``arkts-no-structural-identity``

**Severity: error**

Currently, ArkTS does not support structural identity, i.e., the compiler
cannot compare two types' public APIs and decide whether such types are
identical. Use other mechanisms (inheritance, interfaces or type aliases)
instead.

For the examples below, types ``X`` and ``Y`` are equivalent (interchangeble)
in TypeScript, while in ArkTS they are not.


## TypeScript


```

    interface X {
        f(): string
    }

    interface Y { // Y is equal to X
        f(): string
    }

```

## ArkTS


```

    interface X {
        f(): string
    }

    type Y = X // Y is equal to X

```

## See also

- Recipe 031:  Structural typing is not supported for subtyping / supertyping (``arkts-no-structural-subtyping``)
- Recipe 032:  Structural typing is not supported for assignability checks (``arkts-no-structural-assignability``)
- Recipe 035:  Structural typing is not supported for type inference (``arkts-no-structural-inference``)



