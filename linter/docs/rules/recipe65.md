#  ``instanceof`` operator is partially supported

Rule ``arkts-instanceof-ref-types``

**Severity: error**

In TypeScript, the left-hand side of an ``instanceof`` expression must be of type
``any``, an object type or a type parameter, otherwise the result is ``false``.
In ArkTS, the left-hand side expression may be of any reference type, otherwise
a compile-time error occurs. In addition, the left operand in ArkTS cannot be
a type.


## TypeScript


```

    class X {}

    let a = (new X()) instanceof Object // true
    let b = (new X()) instanceof X // true
    // left operand is a type:
    let c = X instanceof Object // true
    let d = X instanceof X // false

    // left operand is not of type any
    let e = (5.0 as Number) instanceof Number // false

```

## ArkTS


```

    class X {}

    let a = (new X()) instanceof Object // true
    let b = (new X()) instanceof X // true
    // left operand is a type:
    let c = X instanceof Object // Compile-time error
    let d = X instanceof X // Compile-time error

    // left operand may be of any reference type, like number
    let e = (5.0 as Number) instanceof Number // true

```


