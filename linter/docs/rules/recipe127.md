#  Special export type declarations are not supported

Rule ``arkts-no-special-exports``

**Severity: error**

ArkTS does not have a special notation for exporting types.
Use ordinary export instead.


## TypeScript


```

    class C {}
    export type { C }

```

## ArkTS


```

    export class C {}

```


