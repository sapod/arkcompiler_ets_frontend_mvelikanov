#  Only one static block is supported

Rule ``arkts-no-multiple-static-blocks``

**Severity: error**

ArkTS does not allow to have sevaral static block for class initialization,
combine static blocks statements to the one static block.


## TypeScript


```

    class C {
        static s: string

        static {
            C.s = "aa"
        }
        static {
            C.s = C.s + "bb"
        }
    }

```

## ArkTS


```


    class C {
        static s: string

        static {
            C.s = "aa"
            C.s = C.s + "bb"
        }
    }


```


