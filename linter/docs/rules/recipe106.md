#  Constructor function type is not supported

Rule ``arkts-no-ctor-signatures-funcs``

**Severity: error**

ArkTS does not support the usage of the constructor function type.
Use lambdas instead, as they can be generalized to several types of objects.


## TypeScript


```

    class Person {
        constructor(
            name: string,
            age: number
        ) {}
    }

    type PersonConstructor = new (name: string, age: number) => Person

    function createPerson(Ctor: PersonConstructor, name: string, age: number): Person {
        return new Ctor(name, age)
    }

    const person = createPerson(Person, 'John', 30)

```

## ArkTS


```

    class Person {
        constructor(
            name: string,
            age: number
        ) {}
    }

    let PersonConstructor: (name: string, age: number) => Person = (name: string, age: number): Person => {
        return new Person(name, age)
    }

    function createPerson(Ctor: (name: string, age: number) => Person, name: string, age: number): Person {
        return PersonConstructor(name, age)
    }

    function main(): void {
        const person = createPerson(PersonConstructor, "John", 30)
    }

```


