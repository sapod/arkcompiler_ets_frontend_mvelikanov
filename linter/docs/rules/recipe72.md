#  Usage of contextually typed expressions is supported partially

Rule ``arkts-no-contextually-typed-expressions``

**Severity: warning**

In many cases TypeScript allows the use of contextually typed expressions,
where the actual type of expression is defined by the type of destination.
ArkTS allows the use of contextually typed expressions in the following
two situations:

- as an initialization expression in a variable or constant declaration; and
- as an argument in a function call.

Other usages are contradictory to the static typing. There are no implicit casts
from any arbitrary type to another type.


## TypeScript


```

    class Point {
        x: number = 0.0
        y: number = 0.0
    }

    function id_x_y(o: Point): Point {
        return o
    }

    // Structural typing is used to deduce that p is Point:
    let p = {x: 5, y: 10}
    id_x_y(p)

    // A literal can be contextually (i.e., implicitly) typed as Point:
    id_x_y({x: 5, y: 10})

```

## ArkTS


```

    class Point {
        x: number = 0.0
        y: number = 0.0

        // constructor() is used before composite initialization to create a valid object.
        // Since there is no other Point constructors,
        // constructor() is automatically added by compiler
    }

    function id_x_y(o: Point): Point {
        return o
    }

    // Explicit type is required for composite initialization
    let p: Point = {x: 5, y: 10}
    id_x_y(p)

    // id_x_y expects Point explicitly
    // New instance of Point is initialized with composite
    id_x_y({x: 5, y: 10})

```


