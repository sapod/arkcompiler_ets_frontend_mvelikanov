#  Optional parameters are not supported for primitive types

Rule ``arkts-no-opt-params``

**Severity: error**

ArkTS does not support optional parameters of primitive types.
You can use default parameter values or reference types. For reference types,
non-specified optional parameter is set to ``null``.


## TypeScript


```

    // x is an optional parameter:
    function f(x?: number) {
        console.log(x) // log undefined or number
    }

    // x is a required parameter with the default value:
    function g(x: number = 1) {
        console.log(x)
    }

```

## ArkTS


```

    // You can use reference type (will be set to null if missing):
    function f(x?: Number) {
        console.log(x) // log null or some number
    }

    // You can use a required parameter with the default value:
    function g(x: number = 1) {
        console.log(x)
    }

```


