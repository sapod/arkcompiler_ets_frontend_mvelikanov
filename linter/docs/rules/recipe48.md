#  Shortcut syntax for lambdas is not supported

Rule ``arkts-no-shortcut-syntax-in-lambdas``

**Severity: warning**

Currently, ArkTS does not support shortcut syntax for lambdas.


## TypeScript


```

    let a = (x: number) => { return x }
    let b = (x: number) => x

```

## ArkTS


```

    let a: (x: number) => number =
        (x: number): number => { return x }

    let b: (x: number) => number =
        (x: number): number => { return x }

```


