#  Default values for type parameters in generics are not supported

Rule ``arkts-no-default-type-params``

**Severity: warning**

Currently, ArkTS does not support default values for type parameters. Use
generic parameters without default values instead.


## TypeScript


```

    // Declare a generic function:
    function foo<N = number, S = string>() {
        return new Map<N, S>()
    }

    // Call the function:
    foo() // foo<number, string>() will be called

```

## ArkTS


```

    // Declare a generic function:
    function foo<N, S>() {
        return new Map<N, S>()
    }

    // Call the function:
    foo<number, string>() // N and S should be specified explicitly

```


