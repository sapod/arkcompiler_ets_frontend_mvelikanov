#  ``new.target`` is not supported

Rule ``arkts-no-new-target``

**Severity: error**

ArkTS does not support ``new.target`` because there is no concept of runtime
prototype inheritance in the language. This feature is considered not applicable
to the statically typing.


## TypeScript


```

    class CustomError extends Error {
        constructor(message?: string) {
            // 'Error' breaks prototype chain here:
            super(message)

            // Restore prototype chain:
            Object.setPrototypeOf(this, new.target.prototype)
        }
    }

```

## See also

- Recipe 136:  Prototype assignment is not supported (``arkts-no-prototype-assignment``)


