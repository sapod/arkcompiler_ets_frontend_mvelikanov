#  Prototype assignment is not supported

Rule ``arkts-no-prototype-assignment``

**Severity: error**

ArkTS does not support prototype assignment because there is no concept of
runtime prototype inheritance in the language. This feature is considered not
applicable to the static typing.


## TypeScript


```

    var C = function(p) {
        this.p = p
    }

    C.prototype = {
        m() {
            console.log(this.p)
        }
    }

    C.prototype.q = function(r) {
        return this.p === r
    }

```

## See also

- Recipe 132:  ``new.target`` is not supported (``arkts-no-new-target``)


