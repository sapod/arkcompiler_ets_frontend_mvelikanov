#  Renaming in export declarations is not supported

Rule ``arkts-no-export-renaming``

**Severity: error**

ArkTS does not support renaming in export declarations. Similar effect
can be achieved through setting an alias for the exported entity.


## TypeScript


```

    // file1.ts
    class MyClass {
        // ...
    }

    export { MyClass as RenamedClass }

    // file2.ts
    import { RenamedClass } from "./file1"

    function main(): void {
        const myObject = new RenamedClass()
        // ...
    }

```

## ArkTS


```

    // module1
    class MyClass {
        // ...
    }

    export type RenamedClass = MyClass

    // module2
    import { RenamedClass } from "./module1"

    function main(): void {
        const myObject = new RenamedClass()
        // ...
    }

```

## See also

- Recipe 124:  Export list declaration is not supported (``arkts-no-export-list-decl``)
- Recipe 125:  Re-exporting is not supported (``arkts-no-reexport``)
- Recipe 126:  ``export = ...`` assignment is not supported (``arkts-no-export-assignment``)


