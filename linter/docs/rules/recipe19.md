#  Use inheritance instead of intersection types

Rule ``arkts-no-intersection-types``

**Severity: error**

Currently, ArkTS does not support intersection types. You can use inheritance
as a work-around.


## TypeScript


```

    interface Identity {
        id: number
        name: string
    }

    interface Contact {
        email: string
        phone: string
    }

    type Employee = Identity & Contact

```

## ArkTS


```

    interface Identity {
        id: number
        name: string
    }

    interface Contact {
        email: string
        phone: string
    }

    interface Employee extends Identity,  Contact {}

```


