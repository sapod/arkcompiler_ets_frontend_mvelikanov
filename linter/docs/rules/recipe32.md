#  Structural typing is not supported for assignability checks

Rule ``arkts-no-structural-assignability``

**Severity: error**

Currently, ArkTS does not check structural equivalence when checking if types
are assignable to each other, i.e., the compiler cannot compare two types'
public APIs and decide whether such types are identical. Use other mechanisms
(inheritance or interfaces) instead.


## TypeScript


```

    class X {
        public foo: number

        constructor() {
           this.foo = 0
        }
    }

    class Y {
        public foo: number
        constructor() {
            this.foo = 0
        }
    }

    let x = new X()
    let y = new Y()

    console.log("Assign X to Y")
    y = x

    console.log("Assign Y to X")
    x = y

```

## ArkTS


```

    interface Z {
       foo: number
    }

    // X implements interface Z, which makes relation between X and Y explicit.
    class X implements Z {
        public foo: number

        constructor() {
           this.foo = 0
        }
    }

    // Y implements interface Z, which makes relation between X and Y explicit.
    class Y implements Z {
        public foo: number

        constructor() {
           this.foo = 0
        }
    }

    let x: Z = new X()
    let y: Z = new Y()

    console.log("Assign X to Y")
    y = x // ok, both are of the same type

    console.log("Assign Y to X")
    x = y // ok, both are of the same type

```

## See also

- Recipe 030:  Structural identity is not supported (``arkts-no-structural-identity``)
- Recipe 031:  Structural typing is not supported for subtyping / supertyping (``arkts-no-structural-subtyping``)
- Recipe 035:  Structural typing is not supported for type inference (``arkts-no-structural-inference``)


