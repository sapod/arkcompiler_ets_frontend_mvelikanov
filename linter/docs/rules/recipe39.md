#  Object literals must correspond to explicitly declared class or interface

Rule ``arkts-no-mismatched-obj-literals``

**Severity: warning**

ArkTS supports the usage of object literals if the compiler can infer to what
classes or interfaces such literals correspond to. Otherwise, a compile-time
error occurs.

The class or interface can be inferred from a type of the corresponding function
parameter.


## TypeScript


```

    function foo(x: any) {}
    foo({f: 1})

```

## ArkTS


```

    class S {
        f: number
    }

    function foo(s: S) {}

    foo({f: 2})  // ok
    foo({ff: 2}) // Compile-time error, class 'S' does not have field 'ff'

```

## See also

- Recipe 038:  Object literal must correspond to explicitly declared class or interface (``arkts-no-untyped-obj-literals``)
- Recipe 040:  Object literals cannot be used as type declarations (``arkts-no-obj-literals-as-types``)
- Recipe 042:  Array literals must correspond to known array types (``arkts-no-untyped-arr-literals``)
- Recipe 043:  Untyped array literals are not supported (``arkts-no-noninferrable-arr-literals``)


