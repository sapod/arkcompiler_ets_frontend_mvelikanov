#  Wildcards in module names are not supported

Rule ``arkts-no-module-wildcards``

**Severity: error**

ArkTS does not supported wildcards in module names because in |LANG|, import
is a compile-time, not a runtime feature. Use ordinary export syntax instead.


## TypeScript


```

    declare module "*!text" {
        const content: string
        export default content
    }

```

## See also

- Recipe 128:  Ambient module declaration is not supported (``arkts-no-ambient-decls``)
- Recipe 130:  Universal module definitions (UMD) are not supported (``arkts-no-umd``)
- Recipe 131:  ``.js`` extension is not allowed in module identifiers (``arkts-no-js-extension``)


