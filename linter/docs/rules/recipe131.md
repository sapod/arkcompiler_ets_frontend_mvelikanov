#  ``.js`` extension is not allowed in module identifiers

Rule ``arkts-no-js-extension``

**Severity: error**

ArkTS does not allow to use ``.js`` extension in module identifiers because it
has its own mechanisms for interoperating with JavaScript.


## TypeScript


```

    import { something } from "./module.js"

```

## ArkTS


```

    import { something } from "./module"

```

## See also

- Recipe 128:  Ambient module declaration is not supported (``arkts-no-ambient-decls``)
- Recipe 129:  Wildcards in module names are not supported (``arkts-no-module-wildcards``)


