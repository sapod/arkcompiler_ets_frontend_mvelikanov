#  Use ``Object`` instead of union types

Rule ``arkts-no-union-types``

**Severity: error**

Currently, ArkTS provides limited support for union types;
nullable types are supported only in the form ``T | null``.
You can use ``Object`` to emulate the behaviour of the unions in standard TypeScript.


## TypeScript


```

    var x: string | number

```

## ArkTS


```

    let x: Object

    x = 2
    console.log(x)

    x = "2"
    console.log(x)

```


