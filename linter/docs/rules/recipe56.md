#  Unary ``+`` cannot be used for casting to ``number``

Rule ``arkts-no-unary-plus-cast``

**Severity: error**

ArkTS does not support casting from any type to a numeric type
by using the unary ``+`` operator, which can be applied only to
numeric types.


## TypeScript


```

    function returnTen(): string {
        return "-10"
    }

    function returnString(): string {
        return "string"
    }

    let a = +returnTen()    // -10 as number
    let b = +returnString() // NaN

```

## ArkTS


```

    function returnTen(): string {
        return "-10"
    }

    function returnString(): string {
        return "string"
    }

    let a = +returnTen()    // Compile-time error
    let b = +returnString() // Compile-time error

```

## See also

- Recipe 055:  Unary operators ``+``, ``-`` and ``~`` work only on numbers (``arkts-no-polymorphic-unops``)
- Recipe 061:  Binary operators ``*``, ``/``, ``%``, ``-``, ``<<``, ``>>``, ``>>>``, ``&``, ``^`` and ``|`` work only on numeric types (``arkts-no-polymorphic-binops``)
- Recipe 063:  Binary ``+`` operator supports implicit casts only for numbers and strings (``arkts-no-polymorphic-plus``)


