#  Use ``enum`` instead of string literal types

Rule ``arkts-no-str-literal-types``

**Severity: error**

ArkTS does not support string literal types. Use ``enum`` instead.


## TypeScript


```

    type Easing = "ease-in" | "ease-out" | "ease-in-out"

```

## ArkTS


```

    enum Easing {EaseIn, EaseOut, EaseInOut}

```

## See also

- Recipe 142:  ``as const`` assertions are not supported (``arkts-no-as-const``)


