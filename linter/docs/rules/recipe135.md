#  IIFEs as namespace declarations are not supported

Rule ``arkts-no-iife``

**Severity: error**

ArkTS does not support IIFEs as namespace declarations because in |LANG|,
anonymous functions cannot serve as namespaces. Use regular syntax for
namespaces instead.


## TypeScript


```

    var C = (function() {
        function C(n) {
            this.p = n
        }
        return C
    })()
    C.staticProperty = 1

```

## ArkTS


```

    namespace C {
        // ...
    }

```


