#  Array literals must correspond to known array types

Rule ``arkts-no-untyped-arr-literals``

**Severity: warning**

ArkTS supports the usage of array literals if the compiler can infer
to what array types such literals correspond to. Otherwise, a compile-time
error occurs.


## TypeScript


```

    let x = ["aa", "bb"]

```

## ArkTS


```

    let x: string[] = ["aa", "bb"]

```

## See also

- Recipe 038:  Object literal must correspond to explicitly declared class or interface (``arkts-no-untyped-obj-literals``)
- Recipe 039:  Object literals must correspond to explicitly declared class or interface (``arkts-no-mismatched-obj-literals``)
- Recipe 040:  Object literals cannot be used as type declarations (``arkts-no-obj-literals-as-types``)
- Recipe 043:  Untyped array literals are not supported (``arkts-no-noninferrable-arr-literals``)


