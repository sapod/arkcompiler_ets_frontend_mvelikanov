/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class A {
  getName(): string { return 'A'; }
  getType(): string { return 'class'; }
}
class B {
  getName(): string { return 'B'; }
  getType(): string { return 'type'; }
}
function foo(a1: A, ...a2: A[]): void {
}
foo(new B, new A, new B);

let a =  new A;
a = new B;

let b: B = new A;

function bar(bArr: B[]): void {
  bArr[0] = new A;
}

let name = (new B as A).getName();

class C extends B {
  getBase(): string { return 'B'; }
}

function goo(): B {
  return new B;
}
let c: C = goo() as C;

function zoo(b: B): void {
}
zoo(c as B);
