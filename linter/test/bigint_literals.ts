/*
 * Copyright (c) 2022-2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let a = 0n;
a = -10n;
a = 125n;
a = 100_200n;
a = 1_200_300_400_500_600_700_800_900_000_100_200_300_400_500_600n;
a = 0x125n;
a = 0X1F_5A_49n;
a = 0x301D20FA9C500B732AAAAB677631EE0n;
a = 0b101010n;
a = 0B1000_1000_1000_1000_1000n;
a = 0B1010_0101_1010_0101_1111_0000_1010_0101_1010_0101_1111_0000_1010_0101_1010_0101_1111_0000n;
a = 0o1234567n;
a = 0O3214741762451246532427261534n;