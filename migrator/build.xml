<!--
  * Copyright (c) 2022-2022 Huawei Device Co., Ltd.
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
-->

<project name="migrator" default="build" basedir="." xmlns:if="ant:if" xmlns:unless="ant:unless">
    <description>Migration Tool</description>

    <!-- Set global properties for this build -->
    <property name="project.version" value="1.0"/>
    <property name="src.dir" location="src"/>
    <property name="build.dir" location="build"/>
    <property name="build.classes.dir" value="${build.dir}/classes"/>
    <property name="build.src.dir" location="${build.dir}/src"/>
    <property name="out.dir" location="out"/>
    <property name="out.jar" value="migrator-${project.version}.jar"/>
    <property name="test.dir" location="test"/>
    <property name="api.mapper.rules.dir" location="config"/>

    <property name="lib.dir" location="lib"/>
    <property name="antlr4.classpath" value="${lib.dir}/antlr4/antlr-4.9.3-complete.jar"/>

    <property name="staticts.src.dir" location="${src.dir}/com/ohos/migrator/staticTS/parser"/>
    <property name="staticts.gen.dir" location="${build.src.dir}/com/ohos/migrator/staticTS/parser"/>

    <property name="typescript.dir" location="typescript"/>
    <property name="typescript.linter.dir" location="${typescript.dir}/src/linter"/>
    <property name="typescript.build.dir" location="${typescript.dir}/build"/>
    <property name="typescript.out.dir" location="${out.dir}/typescript"/>
    <property name="typescript.linter.testrunner.js" location="${typescript.build.dir}/javascript/src/linter/TestRunner.js"/>

    <!-- Set of all project libraries needed to compile sources -->
    <path id="project.classpath">
        <fileset dir="${lib.dir}"  >
            <include name="**/*.jar"/>
        </fileset>
    </path>

    <!-- Initially we used IntelliJ Idea to build project, and to make it work, the Antlr plugin had to be configured
         to generate lexer/parser class in the same directory as the grammar file (i.e., in 'src' folder). Since most
         of us still use IntelliJ Idea to develop the project, and old configuration persists locally, the Antlr plugin
         may unexpectedly decide to generate files as per old configuration. Thus, to prevent possible interference with
         those generated files, make sure to clean them up from the 'src'.

         NOTE: Delete this target once the above becomes insignificant.
    -->
    <target name="-clean.staticts.src" description="clean up generated files for staticts at 'src'">
        <echo taskname="delete" message="Cleaning up generated files at ${staticts.src.dir}"/>
        <delete>
            <fileset dir="${staticts.src.dir}">
                <include name="*.tokens"/>
                <include name="*.interp"/>
                <include name="StaticTS*Listener.java"/>
                <include name="StaticTS*Visitor.java"/>
                <include name="StaticTSLexer.java"/>
                <include name="StaticTSParser.java"/>
            </fileset>
        </delete>
    </target>

    <target name="-clean.typescript" description="clean up typescript module">
        <delete dir="${typescript.build.dir}"/>
    </target>

    <target name="clean" description="clean up" depends="-clean.staticts.src, -clean.typescript">
        <delete dir="${build.dir}"/>
        <delete dir="${out.dir}"/>
    </target>

    <target name="-prepare">
        <mkdir dir="${build.dir}"/>
        <mkdir dir="${build.classes.dir}"/>
        <mkdir dir="${out.dir}"/>
        <mkdir dir="${out.dir}/config"/>
        <mkdir dir="${typescript.out.dir}"/>
    </target>

    <!-- Run Antlr4 to generate lexer/parser for specified grammar file -->
    <macrodef name="call-antlr">
        <attribute name="src.dir"/>
        <attribute name="gen.dir"/>
        <attribute name="grammar.file"/>
        <attribute name="package"/>
        <sequential>
            <mkdir dir="@{gen.dir}"/>
            <echo taskname="Antlr" message="Generating: @{grammar.file}"/>
            <java classname="org.antlr.v4.Tool"
                  fork="true"
                  failonerror="true"
                  maxmemory="200M"
                  dir="@{gen.dir}">
                <arg value="-visitor"/>
                <arg value="-no-listener"/>
                <arg value="-o"/>
                <arg value="@{gen.dir}"/>
                <arg value="-package"/>
                <arg value="@{package}"/>
                <arg value="@{src.dir}/@{grammar.file}"/>
                <classpath path="${antlr4.classpath}"/>
            </java>
        </sequential>
    </macrodef>

    <target name="-grammar" depends="-prepare" description="generate parsers">
        <call-antlr src.dir="${staticts.src.dir}" gen.dir="${staticts.gen.dir}" grammar.file="StaticTSLexer.g4" package="com.ohos.migrator.staticTS.parser" />
        <call-antlr src.dir="${staticts.src.dir}" gen.dir="${staticts.gen.dir}" grammar.file="StaticTSParser.g4" package="com.ohos.migrator.staticTS.parser" />
    </target>

    <macrodef name="npm-install">
        <sequential>
            <exec executable="cmd" dir="${typescript.dir}" osfamily="windows" failonerror="true">
                <arg value="/c"/>
                <arg value="npm"/>
                <arg value="install"/>
            </exec>
            <exec executable="npm" dir="${typescript.dir}" osfamily="unix" failonerror="true">
                <arg value="install"/>
            </exec>
        </sequential>
    </macrodef>

    <target name="-typescript" description="compile typescript">
        <npm-install/>
    </target>

    <target name="-compile" depends="-compile_without_ts,-typescript" description="compile all sources"/>

    <target name="-compile_without_ts" depends="-prepare,-grammar" description="compile all the sources except TypeScript ones">
        <javac destdir="${build.classes.dir}" fork="true" includeantruntime="false" debug="true">
            <src>
                <pathelement location="${src.dir}"/>
                <pathelement location="${build.src.dir}"/>
            </src>
            <classpath refid="project.classpath"/>
        </javac>
    </target>

    <target name="-copy_lib" description="populate project dependencies">
        <copy todir="${out.dir}" flatten="true">
            <fileset dir="${lib.dir}">
                <include name="**/*.jar"/>
            </fileset>
        </copy>
    </target>

    <target name="-copy_ts_translator" description="copy compiled TS translator">
        <copy todir="${typescript.out.dir}" flatten="false">
            <fileset dir="${typescript.build.dir}" includes="javascript/**/*" excludes="**/TestRunner.js*"/>
            <fileset dir="${typescript.dir}" includes="node_modules/**/*"/>
        </copy>
    </target>

    <target name="-copy_tslinter" description="copy packed TS linter">
        <copy todir="${typescript.out.dir}/javascript/src/linter/" flatten="false">
            <fileset dir="${typescript.dir}" includes="dist/**/*"/>
        </copy>
    </target>


    <!--target name="pack_linter" description="make ts-linter webpackage">	       
	<copy todir="${typescript.out.dir}/javascript/src/linter/">
		<fileset dir="${typescript.linter.dir}/webpackcfg" includes="**/*"/>
        </copy>
	<exec executable="cmd" dir="${typescript.out.dir}/javascript/src/linter/"  osfamily="windows" failonerror="true">
                < !- arg value="- -entry=TypeScriptLinterMain.js"/ - >
		<arg value="/c"/>
		<arg value="webpack"/>
		<arg value="-t"/>
		<arg value="node"/>
	</exec>
	<exec executable="webpack" dir="${typescript.out.dir}/javascript/src/linter/" osfamily="unix" failonerror="true">
		<arg value="-t"/>
		<arg value="node"/>
            </exec>
    </target-->

    <target name="-copy_mapping_rules" description="copy API mapping rule files to the distribution directory">
        <copy todir="${out.dir}/config" flatten="true">
            <fileset dir="${api.mapper.rules.dir}">
                <include name="**/*.xml"/>
            </fileset>
        </copy>
        <copy todir="${build.classes.dir}/config" flatten="true">
            <fileset dir="${api.mapper.rules.dir}">
                <include name="**/*.xml"/>
            </fileset>
        </copy>
    </target>

    <target name="-make_jar" description="create output jar">
        <manifestclasspath property="jar.classpath" jarfile="${out.dir}/migrator-${project.version}.jar">
            <classpath>
                <fileset dir="${out.dir}">
                    <include name="**/*.jar"/>
                    <exclude name="${out.jar}"/>
                </fileset>
            </classpath>
        </manifestclasspath>
        <jar destfile="${out.dir}/${out.jar}" basedir="${build.classes.dir}">
            <manifest>
                <attribute name="Main-Class" value="com.ohos.migrator.Main"/>
                <attribute name="Class-Path" value="${jar.classpath}"/>
            </manifest>
        </jar>
    </target>

    <macrodef name="run-tests">
        <attribute name="lang"/>
        <attribute name="debug" default="false"/>
        <sequential>
            <delete dir="test/@{lang}/results"/>
            <java classname="com.ohos.migrator.TestRunner"
                  fork="true"
                  failonerror="false"
                  maxmemory="200M"
                  outputproperty="test.@{lang}.out"
                  errorproperty="test.@{lang}.err"
                  resultproperty="test.@{lang}.result">
                <arg value="@{lang}"/>
                <jvmarg value="-Dout.dir=${out.dir}"/>
                <jvmarg if:true="@{debug}" value="-agentlib:jdwp=transport=dt_socket,server=n,address=localhost:5005,suspend=y"/>
                <classpath>
                    <fileset dir="${out.dir}">
                        <include name="**/*.jar"/>
                    </fileset>
                </classpath>
            </java>
            <echo message="STDOUT:"/>
            <echo message="${test.@{lang}.out}"/>
            <fail>
    One or several tests have failed!
    STDERR:
    ${test.@{lang}.err}
                <condition>
                    <not>
                        <equals arg1="${test.@{lang}.result}" arg2="0"/>
                    </not>
                </condition>
            </fail>
        </sequential>
    </macrodef>

    <target name="test_java" depends="build_without_ts" description="run Java tests">
        <run-tests lang="java"/>
    </target>

    <target name="test_java_debug" depends="build_without_ts" description="run Java tests in debug mode">
        <run-tests lang="java" debug="true"/>
    </target>

    <target name="test_java_api" description="run Java API mapping tests">
        <run-tests lang="java-mapper"/>
    </target>

    <target name="test_java_api_debug" description="run Java API mapping tests in debug mode">
        <run-tests lang="java-mapper" debug="true"/>
    </target>

    <target name="test_kotlin" depends="build_without_ts" description="run Kotlin tests">
        <run-tests lang="kotlin"/>
    </target>

    <target name="test_kotlin_debug" depends="build_without_ts" description="run Kotlin tests in debug mode">
        <run-tests lang="kotlin" debug="true"/>
    </target>

    <target name="test_ts" depends="build" description="run TypeScript tests">
        <run-tests lang="ts"/>
    </target>

    <target name="test_ts_debug" depends="build" description="run TypeScript tests in debug mode">
        <run-tests lang="ts" debug="true"/>
    </target>

    <target name="test" depends="build" description="run all tests">
        <run-tests lang="java"/>
        <run-tests lang="java-mapper"/>
        <run-tests lang="kotlin"/>
        <run-tests lang="ts"/>
    </target>

    <target name="test_linter" depends="-clean.typescript, -typescript" description="run TS Linter tests">
        <sequential>
            <delete dir="test/linter/results"/>
            <exec executable="cmd" osfamily="windows" failonerror="true">
                <arg value="/c"/>
                <arg value="node"/>
                <arg value="${typescript.linter.testrunner.js}"/>
            </exec>
            <exec executable="node" osfamily="unix" failonerror="true">
                <arg value="${typescript.linter.testrunner.js}"/>
            </exec>
        </sequential>
    </target>

    <target name="build" depends="-compile,-copy_lib,-copy_mapping_rules,-copy_ts_translator,-copy_tslinter,-make_jar"
            description="compile and generate all project files">
        <tstamp>
            <format property="done.tstamp" pattern="yyyy-MM-dd HH:mm:ss"/>
        </tstamp>
        <echo message="Build finished at ${done.tstamp}"/>
    </target>

    <target name="build_without_ts" depends="-compile_without_ts,-copy_lib,-copy_mapping_rules,-make_jar"
            description="compile and generate all project files except TypeScript sources">
        <tstamp>
            <format property="done.tstamp" pattern="yyyy-MM-dd HH:mm:ss"/>
        </tstamp>
        <echo message="Build finished at ${done.tstamp}"/>
    </target>
</project>
