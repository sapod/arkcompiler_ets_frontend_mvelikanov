{
  "type": "Program",
  "statements": [
    {
      "type": "ImportDeclaration",
      "source": {
        "type": "StringLiteral",
        "value": "std/math",
        "loc": {
          "start": {
            "line": 1,
            "column": 24
          },
          "end": {
            "line": 1,
            "column": 34
          }
        }
      },
      "specifiers": [
        {
          "type": "ImportSpecifier",
          "local": {
            "type": "Identifier",
            "name": "V",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 16
              },
              "end": {
                "line": 1,
                "column": 17
              }
            }
          },
          "imported": {
            "type": "Identifier",
            "name": "max",
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 9
              },
              "end": {
                "line": 1,
                "column": 12
              }
            }
          },
          "loc": {
            "start": {
              "line": 1,
              "column": 9
            },
            "end": {
              "line": 1,
              "column": 17
            }
          }
        }
      ],
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 34
        }
      }
    },
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "main",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 3,
                  "column": 10
                },
                "end": {
                  "line": 3,
                  "column": 14
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "main",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 10
                    },
                    "end": {
                      "line": 3,
                      "column": 14
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 18
                    },
                    "end": {
                      "line": 3,
                      "column": 22
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "x",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 9
                              },
                              "end": {
                                "line": 4,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "CallExpression",
                            "callee": {
                              "type": "Identifier",
                              "name": "V",
                              "decorators": [],
                              "loc": {
                                "start": {
                                  "line": 4,
                                  "column": 13
                                },
                                "end": {
                                  "line": 4,
                                  "column": 14
                                }
                              }
                            },
                            "arguments": [
                              {
                                "type": "NumberLiteral",
                                "value": 1,
                                "loc": {
                                  "start": {
                                    "line": 4,
                                    "column": 15
                                  },
                                  "end": {
                                    "line": 4,
                                    "column": 16
                                  }
                                }
                              },
                              {
                                "type": "NumberLiteral",
                                "value": 2,
                                "loc": {
                                  "start": {
                                    "line": 4,
                                    "column": 18
                                  },
                                  "end": {
                                    "line": 4,
                                    "column": 19
                                  }
                                }
                              }
                            ],
                            "optional": false,
                            "loc": {
                              "start": {
                                "line": 4,
                                "column": 13
                              },
                              "end": {
                                "line": 4,
                                "column": 20
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 4,
                              "column": 9
                            },
                            "end": {
                              "line": 4,
                              "column": 20
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 4,
                          "column": 5
                        },
                        "end": {
                          "line": 4,
                          "column": 21
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 3,
                      "column": 23
                    },
                    "end": {
                      "line": 5,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 3,
                    "column": 14
                  },
                  "end": {
                    "line": 5,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 3,
                  "column": 14
                },
                "end": {
                  "line": 5,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 3,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 5,
      "column": 2
    }
  }
}
