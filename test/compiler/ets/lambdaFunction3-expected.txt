{
  "type": "Program",
  "statements": [
    {
      "type": "ClassDeclaration",
      "definition": {
        "id": {
          "type": "Identifier",
          "name": "ETSGLOBAL",
          "decorators": [],
          "loc": {
            "start": {
              "line": 1,
              "column": 1
            },
            "end": {
              "line": 1,
              "column": 1
            }
          }
        },
        "superClass": null,
        "implements": [],
        "body": [
          {
            "type": "MethodDefinition",
            "key": {
              "type": "Identifier",
              "name": "foo",
              "decorators": [],
              "loc": {
                "start": {
                  "line": 1,
                  "column": 10
                },
                "end": {
                  "line": 1,
                  "column": 13
                }
              }
            },
            "kind": "method",
            "accessibility": "public",
            "static": true,
            "optional": false,
            "computed": false,
            "value": {
              "type": "FunctionExpression",
              "function": {
                "type": "ScriptFunction",
                "id": {
                  "type": "Identifier",
                  "name": "foo",
                  "decorators": [],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 10
                    },
                    "end": {
                      "line": 1,
                      "column": 13
                    }
                  }
                },
                "generator": false,
                "async": false,
                "expression": false,
                "params": [],
                "returnType": {
                  "type": "ETSPrimitiveType",
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 17
                    },
                    "end": {
                      "line": 1,
                      "column": 21
                    }
                  }
                },
                "body": {
                  "type": "BlockStatement",
                  "statements": [
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "a",
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 9
                              },
                              "end": {
                                "line": 2,
                                "column": 10
                              }
                            }
                          },
                          "init": {
                            "type": "NumberLiteral",
                            "value": 1,
                            "loc": {
                              "start": {
                                "line": 2,
                                "column": 13
                              },
                              "end": {
                                "line": 2,
                                "column": 14
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 2,
                              "column": 9
                            },
                            "end": {
                              "line": 2,
                              "column": 14
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 2,
                          "column": 5
                        },
                        "end": {
                          "line": 2,
                          "column": 15
                        }
                      }
                    },
                    {
                      "type": "VariableDeclaration",
                      "declarations": [
                        {
                          "type": "VariableDeclarator",
                          "id": {
                            "type": "Identifier",
                            "name": "lambda",
                            "typeAnnotation": {
                              "type": "ETSFunctionType",
                              "params": [
                                {
                                  "type": "Identifier",
                                  "name": "b",
                                  "typeAnnotation": {
                                    "type": "ETSTypeReference",
                                    "part": {
                                      "type": "ETSTypeReferencePart",
                                      "name": {
                                        "type": "Identifier",
                                        "name": "String",
                                        "decorators": [],
                                        "loc": {
                                          "start": {
                                            "line": 3,
                                            "column": 21
                                          },
                                          "end": {
                                            "line": 3,
                                            "column": 27
                                          }
                                        }
                                      },
                                      "loc": {
                                        "start": {
                                          "line": 3,
                                          "column": 21
                                        },
                                        "end": {
                                          "line": 3,
                                          "column": 28
                                        }
                                      }
                                    },
                                    "loc": {
                                      "start": {
                                        "line": 3,
                                        "column": 21
                                      },
                                      "end": {
                                        "line": 3,
                                        "column": 28
                                      }
                                    }
                                  },
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 3,
                                      "column": 18
                                    },
                                    "end": {
                                      "line": 3,
                                      "column": 28
                                    }
                                  }
                                }
                              ],
                              "returnType": {
                                "type": "ETSPrimitiveType",
                                "loc": {
                                  "start": {
                                    "line": 3,
                                    "column": 32
                                  },
                                  "end": {
                                    "line": 3,
                                    "column": 36
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 3,
                                  "column": 17
                                },
                                "end": {
                                  "line": 3,
                                  "column": 36
                                }
                              }
                            },
                            "decorators": [],
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 9
                              },
                              "end": {
                                "line": 3,
                                "column": 15
                              }
                            }
                          },
                          "init": {
                            "type": "ArrowFunctionExpression",
                            "function": {
                              "type": "ScriptFunction",
                              "id": null,
                              "generator": false,
                              "async": false,
                              "expression": false,
                              "params": [
                                {
                                  "type": "Identifier",
                                  "name": "b",
                                  "typeAnnotation": {
                                    "type": "ETSPrimitiveType",
                                    "loc": {
                                      "start": {
                                        "line": 3,
                                        "column": 43
                                      },
                                      "end": {
                                        "line": 3,
                                        "column": 46
                                      }
                                    }
                                  },
                                  "decorators": [],
                                  "loc": {
                                    "start": {
                                      "line": 3,
                                      "column": 40
                                    },
                                    "end": {
                                      "line": 3,
                                      "column": 46
                                    }
                                  }
                                }
                              ],
                              "returnType": {
                                "type": "ETSPrimitiveType",
                                "loc": {
                                  "start": {
                                    "line": 3,
                                    "column": 49
                                  },
                                  "end": {
                                    "line": 3,
                                    "column": 53
                                  }
                                }
                              },
                              "body": {
                                "type": "BlockStatement",
                                "statements": [],
                                "loc": {
                                  "start": {
                                    "line": 3,
                                    "column": 57
                                  },
                                  "end": {
                                    "line": 4,
                                    "column": 6
                                  }
                                }
                              },
                              "loc": {
                                "start": {
                                  "line": 3,
                                  "column": 39
                                },
                                "end": {
                                  "line": 4,
                                  "column": 6
                                }
                              }
                            },
                            "loc": {
                              "start": {
                                "line": 3,
                                "column": 39
                              },
                              "end": {
                                "line": 4,
                                "column": 6
                              }
                            }
                          },
                          "loc": {
                            "start": {
                              "line": 3,
                              "column": 9
                            },
                            "end": {
                              "line": 4,
                              "column": 6
                            }
                          }
                        }
                      ],
                      "kind": "let",
                      "loc": {
                        "start": {
                          "line": 3,
                          "column": 5
                        },
                        "end": {
                          "line": 4,
                          "column": 6
                        }
                      }
                    }
                  ],
                  "loc": {
                    "start": {
                      "line": 1,
                      "column": 22
                    },
                    "end": {
                      "line": 5,
                      "column": 2
                    }
                  }
                },
                "loc": {
                  "start": {
                    "line": 1,
                    "column": 13
                  },
                  "end": {
                    "line": 5,
                    "column": 2
                  }
                }
              },
              "loc": {
                "start": {
                  "line": 1,
                  "column": 13
                },
                "end": {
                  "line": 5,
                  "column": 2
                }
              }
            },
            "overloads": [],
            "decorators": [],
            "loc": {
              "start": {
                "line": 1,
                "column": 1
              },
              "end": {
                "line": 5,
                "column": 2
              }
            }
          }
        ],
        "loc": {
          "start": {
            "line": 1,
            "column": 1
          },
          "end": {
            "line": 1,
            "column": 1
          }
        }
      },
      "loc": {
        "start": {
          "line": 1,
          "column": 1
        },
        "end": {
          "line": 1,
          "column": 1
        }
      }
    }
  ],
  "loc": {
    "start": {
      "line": 1,
      "column": 1
    },
    "end": {
      "line": 6,
      "column": 1
    }
  }
}
TypeError: Initializers type is not assignable to the target type [lambdaFunction3.ets:3:39]
